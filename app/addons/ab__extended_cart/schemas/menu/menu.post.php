<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$schema['central']['ab__addons']['items']['ab__extended_cart'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'position' => 10000,
    'href' => 'ab__extended_cart.help',
    'subitems' => array(
        'ab__extended_cart.settings' => array(
            'href' => 'addons.update&addon=ab__extended_cart',
            'position' => 0
        ),
        'ab__extended_cart.help' => array(
            'href' => 'ab__extended_cart.help',
            'position' => 1000,
        ),
    )
);

return $schema;