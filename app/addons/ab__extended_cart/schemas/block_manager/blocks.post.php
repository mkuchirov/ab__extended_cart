<?php

/*$schema['ab__fast_navigation'] = array (
    'content' => array (
        'items' => array(
            'type' => 'function',
            'function' => array('fn_ab__fn_get_menu_items')
        ),
        'menu' => array(
            'type' => 'template',
            'template' => 'views/menus/components/block_settings.tpl',
            'hide_label' => true,
            'data_function' => array('fn_get_menus'),
        ),
        'ab__fn_show_common_btn' => array(
            'type' => 'template',
            'tooltip' => __('ab__fn_show_common_btn.tooltip'),
            'template' => 'addons/ab__fast_navigation/block_settings/show.tpl'
        ),
        'ab__fn_common_btn_text' => array(
            'type' => 'template',
            'tooltip' => __('ab__fn.common_btn_text.tooltip'),
            'template' => 'addons/ab__fast_navigation/block_settings/text.tpl'
        ),
        'ab__fn_show_common_btn_link' => array(
            'type' => 'template',
            'tooltip' => __('ab__fn_show_common_btn_link.tooltip'),
            'template' => 'addons/ab__fast_navigation/block_settings/link.tpl'
        ),
        'ab__fn_common_btn_class' => array(
            'type' => 'template',
            'tooltip' => __('ab__fn_common_btn_class.tooltip'),
            'template' => 'addons/ab__fast_navigation/block_settings/classes.tpl'
        ),
        'ab__fn_common_btn_type' => array(
            'type' => 'template',
            'tooltip' => __('ab__fn_common_btn_type.tooltip'),
            'template' => 'addons/ab__fast_navigation/block_settings/appearence.tpl'
        )
    ),
    'templates' => array (
        'addons/ab__fast_navigation/blocks/ab__fast_navigation/ab__fn_two_level.tpl' => array(
            'settings' => array(
                'ab__fn_number_of_columns_desktop' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_desktop.tooltip'),
                    'default_value' => 6
                ),
                'ab__fn_number_of_columns_desktop_small' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_desktop_small.tooltip'),
                    'default_value' => 6
                ),
                'ab__fn_number_of_columns_tablet' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_tablet.tooltip'),
                    'default_value' => 5
                ),
                'ab__fn_number_of_columns_tablet_small' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_tablet_small.tooltip'),
                    'default_value' => 3
                ),
                'ab__fn_number_of_columns_mobile' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_mobile.tooltip'),
                    'default_value' => 3
                ),
                'ab__fn_init_second_level_scroll' =>  array (
                    'type' => 'checkbox',
                    'default_value' => 'N'
                ),
                'ab__fn_add_link' =>  array (
                    'type' => 'checkbox',
                    'default_value' => 'Y',
                    'tooltip' => __('ab__fn_add_link.tooltip')
                ),
                'ab__fn_icon_width' => array(
                    'type' => 'input',
                    'default_value' => 100
                ),
                'ab__fn_init_scrollbar' =>  array (
                    'type' => 'checkbox',
                    'default_value' => 'N'
                ),
            ),
        ),
        'addons/ab__fast_navigation/blocks/ab__fast_navigation/ab__fn_one_level.tpl' => array(
            'settings' => array(
                'ab__fn_display_type' => array(
                    'type' => 'selectbox',
                    'values' => array(
                        'ab__fn_scroller' => 'ab__fn_scroller',
                        'ab__fn_grid' => 'ab__fn_grid'
                    ),
                    'default_value' => 'ab__fn_scroller'
                ),
                'ab__fn_number_of_columns_desktop' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_desktop.tooltip'),
                    'default_value' => 6
                ),
                'ab__fn_number_of_columns_desktop_small' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_desktop_small.tooltip'),
                    'default_value' => 6
                ),
                'ab__fn_number_of_columns_tablet' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_tablet.tooltip'),
                    'default_value' => 5
                ),
                'ab__fn_number_of_columns_tablet_small' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_tablet_small.tooltip'),
                    'default_value' => 3
                ),
                'ab__fn_number_of_columns_mobile' =>  array (
                    'type' => 'input',
                    'tooltip' => __('ab__fn_number_of_columns_mobile.tooltip'),
                    'default_value' => 3
                ),
                'ab__fn_icon_width' => array(
                    'type' => 'input',
                    'default_value' => 100
                )
            ),
        )
    ),
    'wrappers' => 'blocks/wrappers',
    'cache' => array(
        'update_handlers' => array('static_data', 'static_data_descriptions')
    ),
    'multilanguage' => true,
);

return $schema;
*/