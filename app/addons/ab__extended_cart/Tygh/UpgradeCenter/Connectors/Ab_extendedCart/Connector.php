<?php

namespace Tygh\UpgradeCenter\Connectors\Ab_extendedCart;

use Tygh\Addons\SchemesManager;
use Tygh\Http;
use Tygh\Registry;
use Tygh\Settings;
use Tygh\Tools\Url;
use Tygh\UpgradeCenter\Connectors\BaseAddonConnector;
use Tygh\UpgradeCenter\Connectors\IConnector;
use Tygh\ABAManager;

class Connector extends BaseAddonConnector implements IConnector
{
    protected $addon_id;
    protected $addon;
    protected $manager;
    protected $s;
    protected $m;
    protected $url;

    //--------------------------------------------------------------------------
    // подготовка данных
    //--------------------------------------------------------------------------
    public function __construct()
    {
        parent::__construct();

        $this->addon_id = /*t*/'ab__extended_cart'/*/t*/;
        $this->s = /*t*/'settings'/*/t*/;
        $this->m = /*t*/'ab__addons_manager'/*/t*/;
        $this->u = /*t*/'https://cs-cart.alexbranding.com/api2/'/*/t*/;
        $this->manager  = /*f*/ABAManager::g_a(/*/f*/$this->m);
        $this->addon    = /*f*/ABAManager::g_a(/*/f*/$this->addon_id);
        if (/*f*/Registry::get(/*/f*//*t*/'addons.'/*/t*/ . $this->m . /*t*/'.build'/*/t*/) == 26){
            $this->addon[$this->addon_id][/*t*/'v'/*/t*/] = $this->addon[$this->addon_id][/*t*/'version'/*/t*/];
            $this->addon[$this->addon_id][/*t*/'c'/*/t*/] = (/*f*/strlen(/*/f*//*f*/Registry::get(/*/f*//*t*/'addons.'/*/t*/ . $this->addon_id . /*t*/'.code'/*/t*/))) ? /*f*/Registry::get(/*/f*//*t*/'addons.'/*/t*/ . $this->addon_id . /*t*/'.code'/*/t*/):/*t*/'--'/*/t*/;
            $this->addon[$this->addon_id][/*t*/'b'/*/t*/] = (/*f*/strlen(/*/f*//*f*/Registry::get(/*/f*//*t*/'addons.'/*/t*/ . $this->addon_id . /*t*/'.build'/*/t*/))) ? /*f*/Registry::get(/*/f*//*t*/'addons.'/*/t*/ . $this->addon_id . /*t*/'.build'/*/t*/):/*t*/'--'/*/t*/;

            $this->manager[$this->m][/*t*/'v'/*/t*/] = $this->manager[$this->m][/*t*/'version'/*/t*/];
            $this->manager[$this->m][/*t*/'c'/*/t*/] = (/*f*/strlen(/*/f*//*f*/Registry::get(/*/f*//*t*/'addons.'/*/t*/ . $this->m . /*t*/'.code'/*/t*/))) ? /*f*/Registry::get(/*/f*//*t*/'addons.'/*/t*/ . $this->m . /*t*/'.code'/*/t*/):/*t*/'--'/*/t*/;
            $this->manager[$this->m][/*t*/'b'/*/t*/] = (/*f*/strlen(/*/f*//*f*/Registry::get(/*/f*//*t*/'addons.'/*/t*/ . $this->m . /*t*/'.build'/*/t*/))) ? /*f*/Registry::get(/*/f*//*t*/'addons.'/*/t*/ . $this->m . /*t*/'.build'/*/t*/):/*t*/'--'/*/t*/;
        }
    }

    //--------------------------------------------------------------------------
    // Проверка модуля - Запрос
    //--------------------------------------------------------------------------
    public function getConnectionData()
    {
        Http::$logging = false;
        return array(
            /*t*/'method'/*/t*/  => /*t*/'post'/*/t*/,
            /*t*/'url'/*/t*/     => $this->u,
            /*t*/'data'/*/t*/    => array(
                /*t*/'r'/*/t*/ => /*t*/'uc.cs'/*/t*/,
                /*t*/'k'/*/t*/ => $this->manager[$this->m][/*t*/'c'/*/t*/],
                /*t*/'b'/*/t*/ => $this->manager[$this->m][/*t*/'b'/*/t*/],
                /*t*/'h'/*/t*/ => /*f*/fn_allowed_for(/*/f*//*t*/'MULTIVENDOR'/*/t*/) ? /*f*/Registry::get(/*/f*//*t*/'config.http_host'/*/t*/) : /*f*/db_get_fields(/*/f*//*t*/"SELECT storefront FROM ?:companies WHERE status = 'A' AND storefront != ''"/*/t*/),
                /*t*/'l'/*/t*/ =>  /*f*/constant(/*/f*//*t*/'CART_LANGUAGE'/*/t*/),
                /*t*/'pv'/*/t*/ => /*f*/constant(/*/f*//*t*/'PRODUCT_VERSION'/*/t*/),
                /*t*/'pe'/*/t*/ => /*f*/constant(/*/f*//*t*/'PRODUCT_EDITION'/*/t*/),
                /*t*/'pb'/*/t*/ => /*f*/constant(/*/f*//*t*/'PRODUCT_BUILD'/*/t*/),
                /*t*/'a'/*/t*/ => $this->addon,
            ),
        );
    }

    //--------------------------------------------------------------------------
    // Проверка модуля - ОТВЕТ
    //--------------------------------------------------------------------------
    public function processServerResponse($response, $show_upgrade_notice){
        $pd = array();
        $rd = /*f*/json_decode(/*/f*/$response, true);

        if (!empty($rd) and !empty($rd[/*t*/'file'/*/t*/])){
            $pd = $rd;
            $pd[/*t*/'name'/*/t*/] = $this->addon[$this->addon_id][/*t*/'name'/*/t*/] . $pd[/*t*/'name'/*/t*/];

            if ($show_upgrade_notice) {
                /*f*/fn_set_notification(/*/f*//*t*/'W'/*/t*/, __(/*t*/'notice'/*/t*/), __(/*t*/'text_upgrade_available'/*/t*/, array(
                    /*t*/'[product]'/*/t*/ => '<b>' . $pd[/*t*/'name'/*/t*/] . '</b>',
                    /*t*/'[link]'/*/t*/ => /*f*/fn_url(/*/f*//*t*/'upgrade_center.manage'/*/t*/)
                )), /*t*/'S'/*/t*/);
            }
        }

        /*remove_line*/ if (file_exists(Registry::get('config.dir.addons') . $this->addon_id . '/debugConnector.php')){ include_once Registry::get('config.dir.addons') . $this->addon_id . '/debugConnector.php'; $pd = processServerResponse($this->addon[$this->addon_id], Registry::get('config.dir.addons') . $this->addon_id . '/');}

        return $pd;
    }

    public function downloadPackage($schema, $package_path)
    {
        $r = array(false, __(/*t*/'text_uc_cant_download_package'/*/t*/));
        $schema['type'] = $schema['id'];
        if (!empty($schema[/*t*/'key'/*/t*/])){
            Http::$logging = false;
            $res = /*f*/fn_put_contents(/*/f*/$package_path, /*f*/Http::post(/*/f*/$this->u, array(/*t*/'r'/*/t*/ => /*t*/'uc.ga'/*/t*/, /*t*/'k'/*/t*/ => $schema[/*t*/'key'/*/t*/],), array(/*t*/'timeout'/*/t*/=>15,)));
            if (!$res || /*f*/strlen(/*/f*/$error = /*f*/Http::getError()/*/f*/))
            { /*f*/fn_rm(/*/f*/$package_path); } else { /*f*/fn_put_contents(/*/f*//*f*/Registry::get(/*/f*//*t*/'config.dir.upgrade'/*/t*/) . /*t*/'packages/'/*/t*/ . $schema[/*t*/'id'/*/t*/] . /*t*/'/schema.json'/*/t*/, /*f*/json_encode(/*/f*/$schema)); $r = array(true, ''); }
        }

        /*remove_line*/ if (file_exists(Registry::get('config.dir.addons') . $this->addon_id . '/debugConnector.php')){ include_once Registry::get('config.dir.addons') . $this->addon_id . '/debugConnector.php'; $r = downloadPackage($schema, $package_path, $this->addon[$this->addon_id], Registry::get('config.dir.addons') . $this->addon_id . '/'); }

        return $r;
    }

    public function onSuccessPackageInstall($content_schema, $information_schema)
    {
        parent::onSuccessPackageInstall($content_schema, $information_schema);

        $s_id = /*f*/db_get_field(/*/f*//*t*/"SELECT section_id FROM ?:settings_sections WHERE name = ?s AND type = 'ADDON'"/*/t*/, $this->addon_id);
        if ($s_id){
            $st_id = /*f*/db_get_field(/*/f*//*t*/'SELECT section_id FROM ?:settings_sections WHERE parent_id = ?i AND name = ?s'/*/t*/, $s_id, $this->s);
            if ($st_id){
                $b = /*f*/db_get_field(/*/f*//*t*/'SELECT value FROM ?:settings_objects WHERE section_id = ?i AND section_tab_id = ?i AND name = ?s'/*/t*/, $s_id, $st_id, /*t*/'build'/*/t*/);
                $b and /*f*/db_query(/*/f*//*t*/'UPDATE ?:settings_objects SET value = ?s WHERE section_id = ?i AND section_tab_id = ?i AND name = ?s'/*/t*/, $information_schema[/*t*/'build'/*/t*/], $s_id, $st_id, /*t*/'build'/*/t*/);
            }
        }
    }
}