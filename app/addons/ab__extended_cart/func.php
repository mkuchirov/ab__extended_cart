<?php

if (!defined('BOOTSTRAP')) {die('Access denied');}

use Tygh\Registry;

foreach (glob(Registry::get("config.dir.addons") . "/ab__fast_navigation/functions/ab__ec.*.php") as $functions) require_once $functions;

function fn_ab__extended_cart_install() {
    /* $objects = array(
        //----------------------------------------------------------------------
        // Меню с иконками -----------------------------------------------------
        //----------------------------------------------------------------------
        array(  "t" => "?:static_data",
            "i" => array(
                array("n" => "ab__fn_menu_status",               "p" => "char(1)         NOT NULL DEFAULT 'N'"),
                array("n" => "ab__fn_label_color",               "p" => "char(7)         NOT NULL DEFAULT '#ffffff'"),
                array("n" => "ab__fn_label_background",          "p" => "char(7)         NOT NULL DEFAULT '#333333'"),
                array("n" => "ab__fn_use_origin_image",          "p" => "char(1)         NOT NULL DEFAULT 'N'")
            )
        ),
        array(  "t" => "?:static_data_descriptions",
            "i" => array(
                array("n" => "ab__fn_label_text",                "p" => "varchar(100)    NOT NULL DEFAULT ''"),
                array("n" => "ab__fn_label_show",                "p" => "char(1)         NOT NULL DEFAULT 'Y'")
            )
        )
    );

    if (!empty($objects) and is_array($objects)){
        foreach ($objects as $o){
            $fields = db_get_fields('DESCRIBE ' . $o['t']);
            if (!empty($fields) and is_array($fields)){
                // добавить новое поле
                if (!empty($o['i']) and is_array($o['i'])){
                    foreach ($o['i'] as $f) {
                        if (!in_array($f['n'], $fields)){
                            // добавить новое поле
                            db_query("ALTER TABLE ?p ADD ?p ?p", $o['t'], $f['n'], $f['p']);
                        }
                    }
                }
            }
        }
    } */
}